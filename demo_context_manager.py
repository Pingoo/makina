import contextlib
import sqlite3


# connect = sqlite3.connect('/tmp/database.sqlite')
# try:
#     try:
#         cursor = connect.cursor()
#         cursor.execute("CREATE TABLE client (id INTEGER, nom VARCHAR, prenom VARCHAR )")
#         connect.commit()
#     except Exception:
#         connect.rollback()
#
#     try:
#         cursor = connect.cursor()
#         cursor.execute("INSERT INTO client(nom, prenom) VALUES (?, ?)", ("THIPHONET", "Stéphane"))
#         connect.commit()
#     except Exception:
#         connect.rollback()
#
# finally:
#     connect.close()
# TODO: Consulter la documentation sur le ContextManager
@contextlib.contextmanager
def transaction(conn):
    try:
        print("J'ouvre une connexion")
        cursor = conn.cursor()
        yield cursor
        print("Je commit ma transaction")
        conn.commit()
    except Exception:
        print("Je rollback ma transaction")


with sqlite3.connect('/tmp/database.sqlite') as conn:
    with transaction(conn) as cur:
        cur.execute(""",,,""")
        cur.execute("INSERT INTO client(nom, prenom) VALUES (?, ?)", ("THIPHONET", "Stéphane"))


@contextlib.contextmanager
def raises(ExceptionType):
    try:
        yield
        raise Exception('On attendait une exception')
    except ExceptionType:
        pass
