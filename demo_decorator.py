import functools
from datetime import datetime

import requests


def debug(active=True):
    def decorator(function):
        @functools.wraps(function)
        def wrapper(*args, **kwargs):
            if active:
                print("focntion : ", function.__name__)
                print("args : ", args)
                print("kwargs : ", kwargs)
            result = function(*args, **kwargs)
            return result

        return wrapper

    return decorator


def time_passed(func):
    print("coucou Mona")

    @functools.wraps(func)
    def wrapper():
        debut = datetime.now()
        print("Je débute", debut)
        result = func()
        fin = datetime.now()
        print("j'ai fini", fin)
        print("j'ai mis ", fin - debut)
        return result

    return wrapper


@time_passed
@debug(active=False)
def get_user():
    response = requests.get('http://randomuser.me/api')
    return response.json()


# get_user = debug(get_user)

# get_user()
# get_user()
